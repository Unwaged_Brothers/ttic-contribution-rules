# TTiC Contribution rules 

this document announce general rules for contribution in collaborative-projects that are including it.\
\
by contributing in the stated project you are agreed to below rules, \
if you think these rules should change, propose them and wait for official response from TTiC


```bash
Version 0.0.0    @7Apr2021
 ```
## Definitions:
* **Active collaboration:** actively completing task for a milestone
* **Active:** available, mature, helping, reporting person

* **Passive collaboration:** non-critical issue hunting, upgrading out of scope modules, generation of unit-test and documentation 
* **partial code:** stage of development when project is not compile able
* **fragile code:** sometimes code do the job and you dont know why
* **untested code:** compiles but not tested at RunTime
* **shade code:** complex and dirty but doing the job 

## Rules

>**General:**
>> * Do not push partial/untested/fragile/shade code (by order)
>> * Be responsible about accepting a task (Active.co)
>> * Respect well-understood design patterns and do not acrobat 
>> * Do not approve merge-request blindly 
>> * Do report every bug at-sight as an issue
>> * Bug is killed when issue is closed with proper report and testsuit
>> * Do task items in order and never edit a single byte from any unrelated area 
>> * Be Active when you are an Active.co
>> * Ask for help when you are facing bottle necks 
>> * Take note while doing tasks and offer your thoughts with them for next milestone
>> * Master-Merge only happens on milestones and no two branch can merge

## Under agreement
you have to respect rules, sometimes unintentional mistakes can be forgiven but being a 'Mustang'  can have fair consequences . 
